# Projet Dev API/Consommation API avec Django, Rest API et Angular : backend Django
 ## Membres du projet
*   Serge (maintainer)
*   Fatima (developer)
*   Rova (developer)
*   Khaly (developer, merge request reviewer)

## Description du projet et cahier des charges
Création d'une maquette de réseau social (RS) d'entraide d'utilisateurs.

### Utilisateur
Le RS comprend plusieurs utilisateurs (user)

*	1 utilisateur est crédité de 10 points à la création du compte
*	1 utilisateur s'authentifie pour accéder à son compte
*	1 utilisateur peut modifier son profil
*	1 utilisateur peut consulter la liste des profils
*	1 utilsateur peut poster une ou plusieurs questions
*	1 utilisateur peut répondre à une ou plusieurs questions
*	1 utilisateur peut voter une seule fois une question ou réponse.
	
### Question
*	1 utilisateur peut poster autant de questions qu'il a de points
*	poster une question retire 1 point à l'utilisateur
*	1 question peut avoir une ou plusieurs étiquettes (tag)
*	1 question peut recevoir des votes de la part des autres utilisateurs
*	1 utilisateur reçoit 1 point par vote sur sa question

### Réponse
*	1 utilisateur peut répondre à une question sans limitation
*	1 utilisateur reçoit 1 point par vote sur sa réponse

## Modèle conceptuel des données

<img src="conceptual_bdd_model.png" alt="modèle conceptuel des données" title="Modèle conceptuel des données">

## API

|   METHODES API    |   DESCRIPTION |   REQUEST |   RESPONSE |
|-------------------|---------------|-----------|------------|
|   POST    |   /chatting/questions/    |   Poster une question |   Body : id_user, content, num_vote (0), created_at, id_tag |
|   POST    |   /chatting/answers/ |    Poster une réponse  |   Body: id_user, id_question, num_vote (0), content, created_at   |
|   POST    |   /chatting/votes/    |   Voter pour une question |   Body : id_user, id_question (Incrémenter num_vote dans les services)    |
|   POST    |   /chatting/votes/    |   Voter pour une réponse  |   Body : id_user, id_response (Incrémenter num_vote dans les services)    |
|   POST    |   Envoyer une demande d’ami   |   Body : id_user1, id_user2, state (false)    |
|   POST    |   /chatting/tags/ |   Créer un tag    |   Body : value_tag    |
|   GET |   /chatting/questions/    |   Afficher les questions  |   rien    |   Liste des questions Json    |
|   GET |   /chatting/questions/id/ |   Afficher une question   |   id (int)    |   Question + Liste des tags   |
|   GET |   /chatting/answers_question/id_question/ |   Afficher les réponses associées à une question  |   id_question (int)   |   Liste des réponses Json |
|   GET |   /profile/me/    |   Information du profil de l’utilisateur connecté |   rien    |   Profile Json    |
|   GET |   /profile/id/    |   Informations de l’utilisateur avec l’identifiant id |   id  |   Profile Json    |
|   GET |   /profile/   |   affiche la liste des profils enregistrés    |   rien    |   Liste Profile Json  |
|   GET |   /profile/friend/    |   Afficher la liste d’amis du profil connecté |   rien    |   liste de Profile Json   |
|   GET |   /profile/friend/id/ |   Profil de l’ami de l’utilisateur courant avec id    |   rien    |   Profile Json    |
|   DELETE  |   /profile/me/    |   désactive le profil courant |   rien    |   Profil désactivé    |
|   DELETE  |   /profile/friend/id/ |   retire l’ami id de la liste d’ami   |   id  |   amis id retiré de votre liste d’amis    |
|   POST    |   /profile/friend/id/ |   Ajouter ami id dans la liste d’amis de l’utilisateur connecté   |   id  |   Profile Json    |
|   PUT |   /profile/me/    |   modifier le profile de l’utilisateur connecté   |   Body : Profile Json |   Enregistrement Profile Json |
|   POST    |   /auth/register/ |   Création d’un compte utilisateur    |   Body : User Json    |   200 OK  |
|   POST    |   /auth/login/    |   Connexion à un compte existant  |   Body : User Json    |   200 OK  |
|   POST    |   /auth/token/refresh |   






