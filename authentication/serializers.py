from rest_framework import serializers
from django.contrib.auth import get_user_model

User = get_user_model()

class UserCreateSerializer(serializers.ModelSerializer):

    password: serializers.CharField(write_only=True, required=True, style={"input_type":"password"})

    class Meta:
        model = User
        fields = [
            "username",
            "email",
            "password",
        ]
        extra_kwargs = {"passowrd":{"write_only": True}}

    def create(self, validated_data, user=None):
        username= validated_data["username"]
        email = validated_data["email"]
        if email and User.objects.filter(email=email).exclude(username=username).exists():
            raise serializers.ValidationError(
                {"email":"L'email existe déja"}
            )
        password = validated_data["password"]
        user = User( username=username, email=email)
        user.set_password(password)
        user.save()
        return user
