from django.contrib import admin
from chatting.models import Question, Answer, Vote, Tag
from userprofile.models import Profile


class QuestionAdmin(admin.ModelAdmin):
    list_display = ('content','id_user', 'nbr_vote', 'created_at', 'id')
    fields = [('content', 'id_user', 'nbr_vote','created_at')]
    autocomplete_fields = ('id_user',)
    search_fields = ('content','id_user__pseudo')
    autocomplete_fields = ('id_user',)


class AnswerAdmin(admin.ModelAdmin):
    list_display = ('id_user','id_question', 'content', 'nbr_vote', 'created_at','id')
    fields = [('id_user','id_question', 'content', 'nbr_vote', 'created_at')]
    autocomplete_fields = ('id_question',)
    search_fields = ('content','id_question__id')
    autocomplete_fields = ('id_question',)


class VoteAdmin(admin.ModelAdmin):
    list_display = ('id_emmiter','id_receiver', 'id_question', 'id_answer')
    fields = [('id_emmiter','id_receiver', 'id_question', 'id_answer')]
    search_fields = ('id_emmiter','id_receiver', 'id_question', 'id_answer')

class TagAdmin(admin.ModelAdmin):
    list_display = ('value_tag',)
    fields = [('value_tag', 'questions')]
    search_fields = ('value_tag',)


# Register your models here.
admin.site.register(Question, QuestionAdmin)
admin.site.register(Answer, AnswerAdmin)
admin.site.register(Vote, VoteAdmin)
admin.site.register(Tag, TagAdmin)
