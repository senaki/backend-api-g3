from django.shortcuts import render
from rest_framework.decorators import api_view

from .models import Question, Answer, Vote, Tag
from .serializers import QuestionSerializer, AnswerSerializer, VoteSerializer, TagSerializer
from rest_framework.response import Response
from userprofile.models import Profile
from rest_framework import viewsets, status
import datetime


class QuestionViewSet(viewsets.ModelViewSet):
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer


@api_view(['GET', 'PUT', 'DELETE'])
def question_detail(request, pk):
    try:
        question = Question.objects.get(pk=pk)
    except Question.DoesNotExist:
        return Response({"error": "question not found"}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = QuestionSerializer(question, context={'request': request})
        return Response(serializer.data)

    if request.method == 'PUT':
        data = request.data
        serializer = QuestionSerializer(question, data=data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_200_OK)

    if request.method == 'DELETE':
        question.delete()
        return Response({"response": "question deleted"}, status=status.HTTP_200_OK)


@api_view(['GET', 'POST'])
def get_questions(request):
    if request.method == 'GET':
        questions = Question.objects.all()
        serializer = QuestionSerializer(questions, many=True, context={'request': request})
        return Response(serializer.data)

    if request.method == 'POST':
        data = request.data
        serializer = QuestionSerializer(data=data, context={'request': request})
        if serializer.is_valid():
            profile = request.user.profile
            if profile.pts > 0:
                profile.pts = profile.pts - 1
                profile.save()
                question = serializer.save()
                question.id_user = profile
                question.nbr_vote = 0
                question.created_at = datetime.datetime.now()
                question.tags.set(data['tags'])
                question.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)

            else:
                return Response({"error": "Not enough points"}, status=status.HTTP_406_NOT_ACCEPTABLE)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
def answer_detail(request, pk):
    try:
        answer = Answer.objects.get(pk=pk)
    except Answer.DoesNotExist:
        return Response({"error": "answer not found"}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = AnswerSerializer(answer, context={'request': request})
        return Response(serializer.data)

    if request.method == 'PUT':
        data = request.data
        serializer = AnswerSerializer(answer, data=data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_200_OK)

    if request.method == 'DELETE':
        answer.delete()
        return Response({"response": "answer deleted"}, status=status.HTTP_200_OK)


@api_view(['GET'])
def get_answers(request):
    if request.method == 'GET':
        answers = Answer.objects.all()
        serializer = AnswerSerializer(answers, many=True, context={'request': request})
        return Response(serializer.data)
'''
if request.method == 'POST':
        data = request.data
        serializer = AnswerSerializer(data=data, context={'request': request})
        if serializer.is_valid():
            profile = request.user.profile
            question = Question.objects.get(pk=data['id_question'])
            answer = serializer.save()
            answer.id_user = profile
            answer.id_question = question
            answer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
'''


@api_view(['GET', 'POST'])
def answers_by_question(request, id_question):
    if request.method == 'GET':
        answers = Answer.objects.filter(id_question=id_question).order_by('-nbr_vote')
        serializer = AnswerSerializer(answers, many=True, context={'request': request})
        if answers:
            return Response(serializer.data)
        else:
            return Response({"error": "no answer found for this question"}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'POST':
        data = request.data
        serializer = AnswerSerializer(data=data, context={'request': request})
        if serializer.is_valid():
            profile = request.user.profile
            question = Question.objects.get(pk=id_question)
            answer = serializer.save()
            answer.id_user = profile
            answer.id_question = question
            answer.nbr_vote = 0
            answer.created_at = datetime.datetime.now()
            answer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
def vote_detail(request, pk):
    try:
        vote = Vote.objects.get(pk=pk)
    except Vote.DoesNotExist:
        return Response({"error": "vote not found"}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = VoteSerializer(vote, context={'request': request})
        return Response(serializer.data)

    if request.method == 'PUT':
        data = request.data
        serializer = VoteSerializer(vote, data=data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_200_OK)

    if request.method == 'DELETE':
        vote.delete()
        return Response({"response": "vote deleted"}, status=status.HTTP_200_OK)



@api_view(['GET'])
def get_votes(request):
    if request.method == 'GET':
        votes = Vote.objects.all()
        serializer = VoteSerializer(votes, many=True, context={'request': request})
        return Response(serializer.data)
'''
    if request.method == 'POST':
        data = request.data
        serializer = VoteSerializer(data=data, context={'request': request})
        to_return = request.data
        status_to_return = status.HTTP_201_CREATED
        if serializer.is_valid():
            try:
                vote = Vote.objects.get(id_emmiter=serializer.initial_data['id_emmiter'],
                                        id_question=serializer.initial_data['id_question'],
                                        id_answer=serializer.initial_data['id_answer'])
                return Response({"error": "user has already voted"}, status=status.HTTP_400_BAD_REQUEST)

            except Vote.DoesNotExist:
                receiver = Profile.objects.get(pk=serializer.initial_data['id_receiver'])
                receiver.pts = receiver.pts + 1
                if serializer.initial_data['id_question'] is not None:
                    question = Question.objects.get(pk=serializer.initial_data['id_question'])
                    if str(serializer.initial_data['id_emmiter']) != str(question.id_user):
                        question.nbr_vote = question.nbr_vote + 1
                        question.save()
                        receiver.save()
                        serializer.save()
                    else:
                        status_to_return = status.HTTP_406_NOT_ACCEPTABLE
                        to_return = {"error": "user can't vote for his own question"}

                elif serializer.initial_data['id_answer'] is not None:
                    answer = Answer.objects.get(pk=serializer.initial_data['id_answer'])
                    if str(serializer.initial_data['id_emmiter']) != str(answer.id_user):
                        answer.nbr_vote = answer.nbr_vote + 1
                        answer.save()
                        receiver.save()
                        serializer.save()
                        print(serializer.initial_data['id_emmiter'])
                    else:
                        status_to_return = status.HTTP_406_NOT_ACCEPTABLE
                        to_return = {"error": "user can't vote for his own answer"}

                return Response(to_return, status=status_to_return)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
'''


@api_view(['GET', 'POST'])
def votes_by_question(request, id_question):
    if request.method == 'GET':
        votes = Vote.objects.filter(id_question=id_question)
        serializer = VoteSerializer(votes, many=True, context={'request': request})
        if votes:
            return Response(serializer.data)
        else:
            return Response({"error": "no vote found for this question"}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'POST':
        data = request.data
        serializer = VoteSerializer(data=data, context={'request': request})
        to_return = request.data
        status_to_return = status.HTTP_201_CREATED
        profile = request.user.profile
        question = Question.objects.get(pk=id_question)
        if serializer.is_valid():
            try:
                vote = Vote.objects.get(id_emmiter=profile.pk,id_question=id_question)
                return Response({"error": "user has already voted"}, status=status.HTTP_400_BAD_REQUEST)

            except Vote.DoesNotExist:
                receiver = Profile.objects.get(pk=question.id_user.pk)
                receiver.pts = receiver.pts + 1
                if str(profile.pk) != str(question.id_user.pk):
                    question.nbr_vote = question.nbr_vote + 1
                    question.save()
                    receiver.save()
                    vote = serializer.save()
                    vote.id_emmiter = profile
                    vote.id_receiver = question.id_user.pk
                    vote.id_question = question
                    vote.id_answer = None
                    vote.save()
                else:
                    status_to_return = status.HTTP_406_NOT_ACCEPTABLE
                    to_return = {"error": "user can't vote for his own question"}
                return Response(to_return, status=status_to_return)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'POST'])
def votes_by_answer(request, id_answer):
    if request.method == 'GET':
        votes = Vote.objects.filter(id_answer=id_answer)
        serializer = VoteSerializer(votes, many=True, context={'request': request})
        if votes:
            return Response(serializer.data)
        else:
            return Response({"error": "no vote found for this answer"}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'POST':
        data = request.data
        serializer = VoteSerializer(data=data, context={'request': request})
        to_return = request.data
        status_to_return = status.HTTP_201_CREATED
        profile = request.user.profile
        answer = Answer.objects.get(pk=id_answer)
        if serializer.is_valid():
            try:
                vote = Vote.objects.get(id_emmiter=profile.pk,id_answer=id_answer)
                return Response({"error": "user has already voted"}, status=status.HTTP_400_BAD_REQUEST)

            except Vote.DoesNotExist:
                receiver = Profile.objects.get(pk=answer.id_user.pk)
                receiver.pts = receiver.pts + 1
                if str(profile.pk) != str(answer.id_user.pk):
                    answer.nbr_vote = answer.nbr_vote + 1
                    answer.save()
                    receiver.save()
                    vote = serializer.save()
                    vote.id_emmiter = profile
                    vote.id_receiver = answer.id_user.pk
                    vote.id_answer = None
                    vote.id_answer = answer
                    vote.save()
                else:
                    status_to_return = status.HTTP_406_NOT_ACCEPTABLE
                    to_return = {"error": "user can't vote for his own answer"}
                return Response(to_return, status=status_to_return)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
def tag_detail(request, pk):
    try:
        tag = Tag.objects.get(pk=pk)
    except Tag.DoesNotExist:
        return Response({"error" : "tag not found"}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = TagSerializer(tag, context={'request': request})
        return Response(serializer.data)

    if request.method == 'PUT':
        data = request.data
        serializer = TagSerializer(tag, data=data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_200_OK)

    if request.method == 'DELETE':
        tag.delete()
        return Response({"response": "tag deleted"}, status=status.HTTP_200_OK)


@api_view(['GET', 'POST'])
def get_tags(request):
    if request.method == 'GET':
        tags = Tag.objects.all()
        serializer = TagSerializer(tags, many=True, context={'request': request})
        return Response(serializer.data)

    if request.method == 'POST':
        data = request.data
        serializer = TagSerializer(data=data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def questions_by_tag(request, id):
    questions = Question.objects.filter(tags=id)
    serializer = QuestionSerializer(questions, many=True, context={'request': request})
    if questions:
        return Response(serializer.data)
    else:
        return Response({"error": "no question found for this tag"}, status=status.HTTP_404_NOT_FOUND)