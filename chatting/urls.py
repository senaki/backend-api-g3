from django.contrib import admin
from django.urls import path, include
from . import views
from rest_framework import routers
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

router = routers.DefaultRouter()
router.register(r'questions', views.QuestionViewSet)

urlpatterns = [
    path('questions/', views.get_questions, name='questions'),
    path('questions/<int:pk>/', views.question_detail, name='question'),
    path('answers/', views.get_answers, name='answers'),
    path('answers/<int:pk>/', views.answer_detail, name='answer'),
    path('answers_question/<int:id_question>/', views.answers_by_question, name='answer_question'),
    path('votes/', views.get_votes, name='votes'),
    path('votes/<int:pk>/', views.vote_detail, name='vote'),
    path('votes_question/<int:id_question>/', views.votes_by_question, name='votes_question'),
    path('votes_answer/<int:id_answer>/', views.votes_by_answer, name='votes_answer'),
    path('tags/', views.get_tags, name='tags'),
    path('tags/<int:pk>/', views.tag_detail, name='tag'),
    path('questions_tag/<int:id>/', views.questions_by_tag, name='question_tag'),

]
