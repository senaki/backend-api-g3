import userprofile.models as user
from django.db import models


# Create your models here.
class Question(models.Model):
    # if user is deleted, his questions/answers are kept
    id_user = models.ForeignKey(
        user.Profile, help_text="User that asks question", on_delete=models.PROTECT, null=True)
    content = models.TextField(
        help_text="Question content", default="Write your question.", editable=True)
    nbr_vote = models.IntegerField(default=0, help_text="Vote")
    created_at = models.DateTimeField(
        auto_created=True, help_text="Creation date", null=True)

    def __str__(self):
        return str(self.pk)+" - "+self.content


class Answer(models.Model):
    # if question is deleted, all answers are removed
    id_user = models.ForeignKey(
        user.Profile, help_text="User that answers", on_delete=models.PROTECT, null=True)
    id_question = models.ForeignKey(
        Question, help_text="Question", on_delete=models.CASCADE, null=True)
    content = models.TextField(
        help_text="Answer content", default="Write your answer.", editable=True)
    nbr_vote = models.IntegerField(default=0, help_text="Vote")
    created_at = models.DateTimeField(
        auto_created=True, help_text="Creation date", null=True)

    def __str__(self):
        return self.content


class Vote(models.Model):
    id_emmiter = models.ForeignKey(
        user.Profile, help_text='Emmiter of the vote', on_delete=models.SET_NULL, null=True)
    id_receiver = models.IntegerField(
        help_text='Receiver id of the vote', null=True)
    id_question = models.ForeignKey(
        Question, help_text='Question which receives the vote', on_delete=models.SET_NULL, null=True, blank=True)
    id_answer = models.ForeignKey(
        Answer, help_text='Answer which receives the vote', on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return str(self.id_emmiter)+" "+str(self.id_receiver)


class Tag(models.Model):
    value_tag = models.CharField(max_length=30, help_text="Tag", null=True)
    questions = models.ManyToManyField(
        "Question", related_name="tags", blank=True)

    def __str__(self):
        return self.value_tag
