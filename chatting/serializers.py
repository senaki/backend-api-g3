from rest_framework import serializers
from .models import Question, Answer, Vote, Tag


class QuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Question
        fields = ('id', 'id_user', 'content', 'nbr_vote', 'created_at')
        depth = 0


class AnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Answer
        fields = ('id', 'id_user', 'id_question', 'content', 'nbr_vote', 'created_at')
        depth = 0;


class VoteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vote
        fields = ('id', 'id_emmiter', 'id_receiver', 'id_question', 'id_answer')
        depth = 0;


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ('id', 'value_tag', 'questions')
        depth = 0;
