from django.contrib.auth import get_user_model
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver


User = get_user_model()

class Profile(models.Model):
    user = models.OneToOneField(User, unique=True, on_delete=models.CASCADE, related_name="profile", null=True, blank=True)
# The following already exist in User Model
    first_name = models.CharField(max_length=255, help_text="Your first name", blank=True, default="")
    last_name = models.CharField(max_length=255, help_text="Your last name", blank=True, default="")
    pts = models.IntegerField(default=10, help_text="Total number of points", editable=False, verbose_name='Point')
    friend = models.ManyToManyField("self", symmetrical=True, blank=True, related_name="friend")

    class Meta:
        ordering = ['user']
        managed = True

    def __str__(self):
        return f'{self.pk}'


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    """ Create user profile for User """
    if created:
        Profile.objects.update_or_create(user=instance, last_name = instance.last_name, first_name = instance.first_name)
        profile = Profile()
    

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    """ Save profile on edit """
    User.first_name = instance.profile.first_name
    User.last_name = instance.profile.last_name
    instance.profile.save()
