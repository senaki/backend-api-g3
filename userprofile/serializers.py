from rest_framework import serializers
from userprofile.models import Profile


# See documentation ModelSerializer
class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ['user', 'first_name', 'last_name', 'pts', 'friend']
