from chatting import serializers
from django.shortcuts import get_object_or_404, render
from rest_framework.decorators import api_view
from userprofile.models import Profile
from rest_framework.response import Response
from rest_framework import viewsets, status
from .serializers import ProfileSerializer
from django.contrib.auth.models import User
from django.contrib.auth import logout

class ProfileViewSet(viewsets.ModelViewSet):
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer

@api_view(['GET'])
def GetProfileById(request, primary_key):
    """ Get the list of the user friends by id """
    try:
        profile = Profile.objects.get(id = primary_key)
    except Profile.DoesNotExist:
        return Response({'error': f"Profile {primary_key} does not exist"}, status=status.HTTP_404_NOT_FOUND)

    serializer = ProfileSerializer(profile)
    """ Return the profile with id = primary_key """
    return Response(serializer.data, status=status.HTTP_200_OK)

@api_view(['GET'])
def GetProfileList(request):
    """ Get the list of the profile """
    try:
        profileList = Profile.objects.all()
    except Profile.DoesNotExist:
        return Response({'error': f"Cannot get the list of profiles"}, status=status.HTTP_400_BAD_REQUEST)

    serializer = ProfileSerializer(profileList, many = True)
    """ Return the profile list """
    return Response(serializer.data, status=status.HTTP_200_OK)

@api_view(['GET'])
def GetProfileById(request, primary_key):
    """ Get profile by id """
    if request.method == 'GET':
        try:
            profile = Profile.objects.get( pk = primary_key)
        except Profile.DoesNotExist:
            return Response({'error': f"Profile {primary_key} does not exist"}, status=status.HTTP_404_NOT_FOUND)

        serializer = ProfileSerializer(profile)
        """ Return the profile with id = primary_key """
        return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['GET'])
def GetProfileList(request):
    """ Get the list of the profile """
    try:
        profileList = Profile.objects.all()
    except Profile.DoesNotExist:
        return Response({'error': f"Cannot get the list of profiles"}, status=status.HTTP_400_BAD_REQUEST)

    serializer = ProfileSerializer(profileList, many=True)
    """ Return the profile list """
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['GET'])
def GetFriendList(request):
    """ Get the list of the user friends by id """
    try:
        emmiter = request.user.profile
    except Profile.DoesNotExist:
        return Response({'error': f"{emmiter.pk} has no friends"}, status=status.HTTP_404_NOT_FOUND)

    friendList = emmiter.friend
    serializer = ProfileSerializer(friendList, many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)

"""
# @api_view(['POST'])
def AddFriend(request, primary_key):
"""
""" Add friend to user friends list """
"""
    try:
        receiver = Profile.objects.get(id=primary_key)
    except Profile.DoesNotExist:
        return

    if receiver in emmiter.friend.all():
        return Response({"response": f"friend {primary_key} is already in your list !"}, status=status.HTTP_208_ALREADY_REPORTED)
    else:
        emmiter.friend.add(receiver)
        emmiter.save()
        serializer = ProfileSerializer(emmiter)
        return Response({"response": "friend added!"}, status=status.HTTP_200_OK)


# @api_view(['DELETE'])
def RemoveFriend(request, primary_key):
"""
""" Remove friend from user friends list """
"""
    emmiter = request.user.profile
    try:
        receiver = Profile.objects.get(pk=primary_key)
    except Profile.DoesNotExist:
        return Response({'error': f"{primary_key} doesn't exist"}, status=status.HTTP_404_NOT_FOUND)

    if receiver in emmiter.friend.all():
        emmiter.friend.remove(receiver)
        emmiter.save()
        return Response({'response': f"Friend {primary_key} removed from your list"}, status=status.HTTP_200_OK)
    else:
        return Response({"response": f"Friend {primary_key} is not in your list !"}, status=status.HTTP_208_ALREADY_REPORTED)


# @api_view(['PUT'])
def UpdateProfile(request):
"""
""" Update user profile """
"""
    try:
        emmiter = request.user.profile
    except Profile.DoesNotExist:
        return Response({'error': f"{request.user.id} doesn't exist"}, status=status.HTTP_404_NOT_FOUND)
    data = request.data
    serializer = ProfileSerializer(emmiter, data=data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)
    else:
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# @api_view(['DELETE'])
def DisableProfile(request):
    try:
        user = request.user
    except User.DoesNotExist:
        return Response({"error": "Does note exist."}, status=status.HTTP_404_NOT_FOUND)

    user.is_active = False
    user.save()
    logout(request)
    return Response({"response": "Profile deleted."}, status=status.HTTP_200_OK)
"""

@api_view(['GET', 'POST', 'DELETE'])
def ManageFriendList(request, primary_key):
    """ Get the list of the user friends by id """
    emmiter = request.user.profile
    if request.method == 'GET':
        try:    
            friend = Profile.objects.get( pk = primary_key)
        except Profile.DoesNotExist:
            return Response({'error': f"{primary_key} doesn't exist"}, status=status.HTTP_404_NOT_FOUND)
        serializer = ProfileSerializer(friend)
        return Response(serializer.data, status=status.HTTP_200_OK)

    elif request.method == 'POST':
        """ Add friend to user friends list """
        try:
            receiver = Profile.objects.get( pk = primary_key)
        except Profile.DoesNotExist:
            return Response({'error': f"{primary_key} doesn't exist"}, status=status.HTTP_404_NOT_FOUND)

        if receiver in emmiter.friend.all() :
            return Response({"response": f"friend {primary_key} is already in your list !"}, status=status.HTTP_208_ALREADY_REPORTED)
        if receiver == emmiter :
            return Response({"response": f"You cannot add yourself !"}, status=status.HTTP_400_BAD_REQUEST)
        else:
           emmiter.friend.add(receiver)
           emmiter.save()
           serializer = ProfileSerializer(emmiter)
           return Response({"response": "friend added!"}, status=status.HTTP_200_OK)

    elif request.method == 'DELETE':
        """ Remove friend from user friends list """
        try:
            receiver = Profile.objects.get( pk = primary_key)
        except Profile.DoesNotExist:
            return Response({'error': f"{primary_key} doesn't exist"}, status=status.HTTP_404_NOT_FOUND)

        if receiver in emmiter.friend.all() :
            emmiter.friend.remove(receiver)
            emmiter.save()
            return Response({'response': f"Friend {primary_key} removed from your list"}, status=status.HTTP_200_OK)
        else:
            return Response({"response": f"Friend {primary_key} is not in your list !"}, status=status.HTTP_208_ALREADY_REPORTED)


@api_view(['GET', 'PUT', 'DELETE'])
def ManageProfile(request):
    """ Method to manage profile """
    try:
        myProfile = Profile.objects.get( pk = request.user.profile.pk)
    except Profile.DoesNotExist:
        return Response({'error': f"Profile {request.user.profile.user} does not exist"}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET' :
        """ Get profile by id """
        serializer = ProfileSerializer(myProfile)
        """ Return the profile with id = primary_key """
        return Response(serializer.data, status=status.HTTP_200_OK)

    elif request.method == 'PUT':
        data = request.data
        serializer = ProfileSerializer(myProfile, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    elif request.method == 'DELETE':
        try:
            user = request.user
        except User.DoesNotExist:
            return Response({"error": "Does note exist."}, status=status.HTTP_404_NOT_FOUND)

        user.is_active = False
        user.save()
        logout(request)
        return Response({"response": "Profile deleted."}, status=status.HTTP_200_OK)
