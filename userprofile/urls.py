from django.contrib import admin
from django.urls import path, include
from . import views
from rest_framework import routers
from django.views.generic import RedirectView

router = routers.DefaultRouter()
router.register(r'profile', views.ProfileViewSet)
urlpatterns = [
    path('me/', views.ManageProfile, name='my-profile'),
    path('', views.GetProfileList, name='list-profile'),
    path('<int:primary_key>/', views.GetProfileById, name='id-profile'),
    path('friend/', views.GetFriendList, name='friend-list'),
    path('friend/<int:primary_key>/', views.ManageFriendList, name='friend-list'),
]
