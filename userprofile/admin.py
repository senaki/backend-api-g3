from django.contrib import admin
from userprofile.models import Profile
# Register your models here.

class ProfileAdmin(admin.ModelAdmin):
    list_display = ('user', 'first_name', 'last_name', 'pts')
    fields = [('user', 'pts', ), 'first_name','last_name']
    readonly_fields = ('pts',)
    search_fields = ('first_name', 'last_name')
    list_filter = ('user', 'pts',)

admin.site.register(Profile, ProfileAdmin)
